import React, { Children } from 'react';
import { render, screen } from '@testing-library/react';
import LoginPage from './components/LoginPage';


const routeComponentPropsMock = {
  history: {} as any,
  location: {} as any,
  match: {} as any
}

describe("<LoginPage />", () => {

  const testValues = {
    username: 'FOO',
    password: 'BAZ',
    handleSubmit: jest.fn()
  }



  it("OnSubmit is called when all fields pass validation", () => {
    const component = render(<LoginPage {...routeComponentPropsMock}/> );
    
    console.log(component)
  })
})
