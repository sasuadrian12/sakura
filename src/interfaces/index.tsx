export default interface IPage {
    name: string;
}
export default interface IPrivateRoute {
    key: Number
    path: String;
    component: any
}