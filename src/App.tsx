import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import "./App.css";
import 'antd/dist/antd.css';
import './index.css';
import PrivateScreen from "./pages/PrivateScreen";
import LoginPage from "./components/LoginPage";
import PageNotFound from "./components/PageNotFound";
import PrivateRoute from "./pages/PrivateRoute";
import LayoutComponent from "./components/LayoutComponent";
import TestIt from './pages/TestIt';
import * as API from "./api/index"
//We will remove this.

const App: React.FunctionComponent<{}> = () => {
const [error, setError] = useState<string>("");

useEffect(() => {
  let isTokenExist = true;

  const fetchPrivateData = async () => {
      const config = {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("authToken")}`,
          },
        };

    try {
      const { data }: any = await API.getPrivateRoute(config);
    } catch (error) {
      localStorage.removeItem("authToken");
      setError("You are not authorized please login");
    }
    //clean unsubscription
    return () => (isTokenExist = false)
  };

  fetchPrivateData();
},[])

  return (
    <React.Fragment>
      <Router>
        <Switch>
        {error ? (<Route exact path={'/login'} component={LoginPage} />): (< Redirect to={"/login"}/>)}
          <LayoutComponent >
            <PrivateRoute exact path={'/'} component={PrivateScreen}/>
            <PrivateRoute exact path={'/test'} component={TestIt} />
          </LayoutComponent>
        <Route path={'/*'} component={PageNotFound} />
        </Switch>
      </Router>
    </React.Fragment>
  );
};

export default App;
