import React from "react";
import { Redirect, Route } from "react-router";

interface IPrivateRoute {
  component: any
  exact: boolean
  path: string
}

const PrivateRoute: React.FC<IPrivateRoute> = ({component: Component, ...rest}) => {
  return (
    <React.Fragment>
      <Route
        {...rest}
        render={(props) => 
          localStorage.getItem("authToken") ? 
          (<Component {...props} />) : 
          (<Redirect to={"/login"}/>)
        }
      />
    </React.Fragment>
  );
};

export default PrivateRoute;
