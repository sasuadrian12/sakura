import React from 'react';
import IPage from '../interfaces/index';
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const Home: React.FC<IPage> = (props) => {

    const menu = (
        <Menu>
            <Menu.Item key="0">
                Rent
            </Menu.Item>
            <Menu.Item key="1">
                Energy
            </Menu.Item>
            <Menu.Item key="2">
                Gas
            </Menu.Item>
        </Menu>
    );

    return (
        <div>
            <Dropdown overlay={menu} trigger={['click']}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()} style={{fontSize: 20}}>
                    Select a Need <DownOutlined />
                </a>
            </Dropdown>
        </div>
    )
}

export default Home;