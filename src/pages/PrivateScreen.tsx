import React, { useEffect, useState } from "react";
import IPage from "../interfaces/index";

import {
  RouteComponentProps,
  Link
} from "react-router-dom";

//Api
import * as API from "../api/index"

const PrivateScreen: React.FC<IPage & RouteComponentProps<any>> = ({
  history,
}) => {
  const [error, setError] = useState<string>("");

  useEffect(() => {
    let isTokenExist = true;

    if (!localStorage.getItem("authToken")) {
      history.push("/login");
    }

    const fetchPrivateData = async () => {
        const config = {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("authToken")}`,
            },
          };

      try {
        const { data }: any = await API.getPrivateRoute(config);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
      //clean unsubscription
      return () => (isTokenExist = false)
    };

    fetchPrivateData();
  }, [history]);

  return <React.Fragment>
      Welcome Page
      </React.Fragment>
  
};

export default PrivateScreen;
