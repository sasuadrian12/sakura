import React, { useState } from 'react';
import IPage from '../interfaces/index';
import { RouteComponentProps } from 'react-router-dom';
import ModalComponent from '../components/ModalComponent';
import { Button } from 'antd'

const TestIt: React.FC<IPage & RouteComponentProps<{}>> = ( props ) => {
    const [ isModalVisible, setIsModalVisible ] = useState<boolean>(false);
    const showModal = () => {
        setIsModalVisible(true);
      }
    
      const handleOk = () => {
        // here will be a call to backend
    
        setIsModalVisible(false);
      }
      
      const handleCancel = () => {
        setIsModalVisible(false)
      }
      
    return (
        <div>
            <p>Table page</p>
          <Button onClick={showModal} type={"primary"} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}> Add Data</Button>
          <ModalComponent visibile={isModalVisible} onOk={handleOk} onCancel={handleCancel}/>

        </div>
    )
}

export default TestIt