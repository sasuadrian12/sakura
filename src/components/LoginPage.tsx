import React, { useEffect, useState } from "react";
import IPage from "../interfaces/index";
import { RouteComponentProps } from "react-router-dom";
import { Spin } from 'antd';

//Api
import * as API from "../api/index";

// Chakra imports
import {
  Box,
  Flex,
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Text,
  useColorModeValue,
  useToast
} from "@chakra-ui/react";

//Animations
import { motion } from 'framer-motion';

const LoginPage: React.FC<RouteComponentProps<object>> = ({ history }) => {
  console.log(history)
  const initialStateFormLogin = { email: '', password: '' };


  const [form, setForm] = useState<object>(initialStateFormLogin);
  const [loading, setLoading] = useState<boolean>(false);

  // Chakra color mode
  const titleColor = useColorModeValue("teal.300", "teal.200");
  const textColor = useColorModeValue("gray.400", "white");
  const toast = useToast();

  useEffect(() => {
    if (localStorage.getItem("authToken")) {
      history.push("/");
    }
  }, [history]);

 const loginHandler = async (e: any) => {
    e.preventDefault();
    const config: object = {
      header: {
        "Content-Type": "application/json",
      },
    };

    try {
      setLoading(true)
      const { data }: any = await API.login(form, config)
      localStorage.setItem("authToken", data.token);
      history.push("/");

      toast({
        title: "You're in!",
        description: "You got acces to the private data in this route",
        status: "success",
        duration: 3000,
        isClosable: true,
    })
    } catch (error: any) {
      setLoading(false)
      toast({
        title: error.response.data.error,
        status: "error",
        duration: 1000,
        isClosable: true,
    })
    }
  };

  const handleChange = (e: any) => setForm({ ...form, [e.target.name]: e.target.value })

  return (
    <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{delay: 0.5, duration: 0.5}}
    >
      <Flex position="relative" mb="40px">
        <Flex
          h={{ sm: "initial", md: "75vh", lg: "85vh" }}
          w="100%"
          maxW="1044px"
          mx="auto"
          justifyContent="space-between"
          mb="30px"
          pt={{ sm: "100px", md: "0px" }}
        >
          <Flex
            alignItems="center"
            justifyContent="start"
            style={{ userSelect: "none" }}
            w={{ base: "100%", md: "50%", lg: "42%" }}
          >
            <Flex
              direction="column"
              w="100%"
              background="transparent"
              p="48px"
              mt={{ md: "150px", lg: "80px" }}
            >
              <Heading color={titleColor} fontSize="32px" mb="10px">
                Welcome
              </Heading>
              <Text
                mb="36px"
                ms="4px"
                color={textColor}
                fontWeight="bold"
                fontSize="14px"
              >
                Enter your email and password to sign in
              </Text>
              <form onSubmit={loginHandler}>
                <FormControl>
                  <FormLabel ms="4px" fontSize="sm" fontWeight="normal">
                    Email
                  </FormLabel>
                  <Input
                    borderRadius="15px"
                    mb="24px"
                    fontSize="sm"
                    type="email"
                    placeholder="Your email adress"
                    size="lg"
                    required
                    id={"email"}
                    name={"email"}
                    onChange={handleChange}
                  />
                  <FormLabel ms="4px" fontSize="sm" fontWeight="normal">
                    Password
                  </FormLabel>
                  <Input
                    borderRadius="15px"
                    mb="36px"
                    fontSize="sm"
                    type="password"
                    placeholder="Your password"
                    size="lg"
                    required
                    id={"password"}
                    name={"password"}
                    onChange={handleChange}
                  />
                  {/* <FormControl display="flex" alignItems="center">
                            <Switch id="remember-login" colorScheme="teal" me="10px" />
                            <FormLabel
                            htmlFor="remember-login"
                            mb="0"
                            ms="1"
                            fontWeight="normal"
                            >
                            Remember me
                            </FormLabel>
                        </FormControl> */}
                  <Button
                    fontSize="10px"
                    type="submit"
                    bg="teal.300"
                    w="100%"
                    h="45"
                    mb="20px"
                    color="white"
                    mt="20px"
                    _hover={{
                      bg: "teal.200",
                    }}
                    _active={{
                      bg: "teal.400",
                    }}
                  >
                    {loading ? <Spin size="large" /> : 'Sing up'}
                  </Button>
                </FormControl>
              </form>
              <Flex
                flexDirection="column"
                justifyContent="center"
                alignItems="center"
                maxW="100%"
                mt="0px"
              >
                {/* <Text color={textColor} fontWeight="medium">
                  Don't have an account?
                  <Link
                    color={titleColor}
                    type={"submit"}
                    as="span"
                    ms="5px"
                    fontWeight="bold"
                  >
                    Sign Up
                  </Link>
                </Text> */}
              </Flex>
            </Flex>
          </Flex>
          <Box
            display={{ base: "none", md: "block" }}
            overflowX="hidden"
            h="100%"
            w="40vw"
            position="absolute"
            right="0px"
          >
            <Box
              w="100%"
              h="100%"
              bgSize="cover"
              bgPosition="50%"
              position="absolute"
              borderBottomLeftRadius="20px"
            ></Box>
          </Box>
        </Flex>
      </Flex>
    </motion.div>
  );
};

export default LoginPage;
