import React from 'react';
import IPage from '../interfaces/index';
import { RouteComponentProps, withRouter } from 'react-router-dom'

const PageNotFound: React.FC<IPage & RouteComponentProps<{}>> = ( props ) => {
    return (
        <div>
            <p>PageNotFound</p>
        </div>
    )
}

export default withRouter(PageNotFound)