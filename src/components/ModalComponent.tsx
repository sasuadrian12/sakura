import React from 'react';
import { Modal } from 'antd';


interface IModal {
    visible: boolean,
    onOk: boolean,
    onCancel: boolean
}

const ModalComponent: React.FC<any> = ({ visibile, onOk, onCancel }) => {
    return (
        <>
            <Modal title="Basic Modal" visible={visibile} onOk={onOk} onCancel={onCancel}>
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </>
    )
}

export default ModalComponent;