import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { Layout, Menu, Breadcrumb, Button } from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
  LogoutOutlined
} from '@ant-design/icons';
import { useHistory } from "react-router";

const LayoutComponent: React.FC<any> = ({ children }) => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const history = useHistory();
  const onCollapse = () => setCollapsed(!collapsed)

  const {  Content, Footer, Sider } = Layout;
  const { SubMenu } = Menu;

  const logoutHandler = () => {
    localStorage.removeItem("authToken")
    history.push("/login");
    window.location.reload();
  };


  return (
    <Layout style={{ minHeight: '100vh'}}>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <div className="logo" style={{ display: 'flex', justifyContent: 'center' }}>
          <Button onClick={logoutHandler} icon={<LogoutOutlined />} />
        </div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1" icon={<PieChartOutlined />}>
            <Link to={"/"} />
            Dashboard
          </Menu.Item>
          <Menu.Item key="2" icon={<DesktopOutlined />}>
            <Link to={"/test"} />
            Option 2
          </Menu.Item>
          <SubMenu key="sub1" icon={<UserOutlined />} title="User">
            <Menu.Item key="3">Tom</Menu.Item>
            <Menu.Item key="4">Bill</Menu.Item>
            <Menu.Item key="5">Alex</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
            <Menu.Item key="6">Team 1</Menu.Item>
            <Menu.Item key="8">Team 2</Menu.Item>
          </SubMenu>
          <Menu.Item key="9" icon={<FileOutlined />}>
            Files
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: '0 16px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            {/* <Breadcrumb.Item>User</Breadcrumb.Item>
            <Breadcrumb.Item>Bill</Breadcrumb.Item> */}
          </Breadcrumb>
          <div className="site-layout-background" style={{ padding: 24, minHeight: '100%' }}>
            {children}

          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>©2021 Created by Dida</Footer>
      </Layout>
    </Layout>
  );
};

export default LayoutComponent;
