import axios from "axios";


const url: String = "https://sasuke-sasuadrian12.vercel.app/api";

export const getPrivateRoute: any = (configHeader: Object) => axios.get(`${url}/private`, configHeader);
export const login: any = (login: Object, configHeader: Object) => axios.post(`${url}/auth/login`, login, configHeader);